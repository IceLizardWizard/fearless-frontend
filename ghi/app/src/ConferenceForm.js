import React, {useEffect, useState} from 'react';

function ConferenceForm () {
  const [name, setName] = useState('');
  const [start, setStart] = useState('');
  const [end, setEnd] = useState('');
  const [description, setDescription] = useState('');
  const [maxPresentations, setMaxPresentations] = useState('');
  const [maxAttendees, setMaxAttendees] = useState('');
  const [locations, setLocations] = useState([]);
  const [location, setLocation] = useState('');
    const fetchData = async () => {
        const url = 'http://localhost:8000/api/locations/'


        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations);
            // setLocations = (data.locations.name)
            // const selectTag = document.getElementById('location');
            // for (let location of data.locations) {
            //     const option = document.createElement('option');
            //     option.value = location.name;
            //     option.innerHTML = location.name;
            //     selectTag.appendChild(option);
            // }
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const handleNameChange = (event) => {
      const value = event.target.value;
      setName(value);
    }
    const handleStart = (event) => {
      const value = event.target.value;
      setStart(value);
    }
    const handleEnd = (event) => {
      const value = event.target.value;
      setEnd(value);
    }
    const handleDescription = (event) => {
      const value = event.target.value;
      setDescription(value);
    }
    const handleMaxPresentations = (event) => {
      const value = event.target.value;
      setMaxPresentations(value);
    }
    const handleMaxAttendees = (event) => {
      const value = event.target.value;
      setMaxAttendees(value);
    }
    const handleLocationChange = (event) => {
      const value = event.target.value;
      setLocation(value);
      console.log(location)
    }

    const handleSubmit = async (event) => {
      event.preventDefault();
      const data = {};
      data.name = name;
      data.starts = start;
      data.ends = end;
      data.description = description;
      data.max_presentations = maxPresentations;
      data.max_attendees = maxAttendees;
      data.location = location;

      console.log(data)

      const conferenceUrl = 'http://localhost:8000/api/conferences/'
      const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
          'Content-Type': 'application/json',
        },
      };
      const response = await fetch(conferenceUrl, fetchConfig);
      if (response.ok) {
        const newConference = await response.json();
        console.log(newConference)

        setName('');
        setStart('');
        setEnd('');
        setDescription('');
        setMaxPresentations('');
        setMaxAttendees('');
        setLocation('');

      }
    }



    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input value={name} onChange={handleNameChange} placeholder="Name" required type="text" id="name" className="form-control"/>
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input value={start} onChange={handleStart} placeholder="Starts" required type="date" id="starts" className="form-control"/>
                <label htmlFor="start">Starts</label>
              </div>
              <div className="form-floating mb-3">
                <input value={end} onChange={handleEnd} placeholder="Ends" required type="date" id="ends" className="form-control"/>
                <label htmlFor="ends">Ends</label>
              </div>
              <div className="form-floating mb-3">
                <input value={description} onChange={handleDescription} placeholder="Description" required type="text" id="description" className="form-control"/>
                <label htmlFor="description">Description</label>
              </div>
              <div className="form-floating mb-3">
                <input value={maxPresentations} onChange={handleMaxPresentations} placeholder="Max presentations" required type="number" id="max_presentations" className="form-control"/>
                <label htmlFor="max_presentations">Max presentations</label>
              </div>
              <div className="form-floating mb-3">
                <input value={maxAttendees} onChange={handleMaxAttendees} placeholder="Max attendees" required type="number" id="max_attendees" className="form-control"/>
                <label htmlFor="max_attendees">Max attendees</label>
              </div>
              <div className="mb-3">
                <select onChange={handleLocationChange} required name="location" id="location" className="form-select">
                  <option value="">Choose a location</option>
                  {locations.map(location => {
                    return (
                        <option key={location.name} value={location.id}>
                            {location.name}
                        </option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
}

export default ConferenceForm;