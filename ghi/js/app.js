window.addEventListener('DOMContentLoaded', async () => {

    function createCard(name, description, pictureUrl, date, dateEnd, location) {
        return `

          <div class="col justify-content-evenly">
            <div class="card shadow p-3 mb-5 bg-body-tertiary rounded" >
                <img src="${pictureUrl}" class="card-img-top">
                <div class="card-body">
                <h5 class="card-title">${name}</h5>
                <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
                <p class="card-text">${description}</p>
                </div>
                <div class="card-footer text-muted">${date} - ${dateEnd}
                </div>
            </div>
          </div>
        `;
      }

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        // Figure out what to do when the response is bad
        console.log(response)

      } else {
        const data = await response.json();

        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const title = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const date = new Date(details.conference.starts).toLocaleDateString();
            const dateEnd = new Date(details.conference.ends).toLocaleDateString();
            const location = details.conference.location.name
            const html = createCard(title, description, pictureUrl, date, dateEnd, location);
            const row = document.querySelector('.row');
            row.innerHTML += html;


            console.log(details);
          }
        }

      }
    } catch (e) {
        console.error(e);
      // Figure out what to do if an error is raised
    }

  });